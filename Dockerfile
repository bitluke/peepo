FROM maven:3.5.0-jdk-8

LABEL vendor=bitfingers\
      com.bitfingers.is-beta= \
      com.bitfingers.is-production="" \
      com.bitfingers.version="1.0.0" \
      com.bitfingers.release-date="2017-09-25"

RUN apt-get update \
  && apt-get install -y postgresql postgresql-contrib \
  && apt-get install sudo \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

EXPOSE 8080
EXPOSE 5432
