INSERT INTO pipouser VALUES
  (nextval('demo_sequence'), 'add', 10, 'city', 'email@email.com', 'fname', 'lname', 'mname', 'pass', TRUE, TRUE,
   'email@email.com');

INSERT INTO pipouser VALUES
  (nextval('demo_sequence'), 'add', 20, 'city', 'email2@email.com', 'fname2', 'lname2', 'mname2', 'pass2', TRUE, TRUE,
   'email2@email.com');

INSERT INTO authority (id, name, description) VALUES (nextval('demo_sequence'), 'ADMIN', 'Administrator role');
INSERT INTO authority (id, name, description) VALUES (nextval('demo_sequence'), 'USER', 'default user');


INSERT INTO pipouser_x_authority (pipouser_id, authority_id) VALUES (
  (SELECT id
   FROM pipouser
   WHERE email = 'email@email.com'),
  (SELECT id
   FROM authority
   WHERE
     name = 'ADMIN'));

INSERT INTO pipouser_x_authority (pipouser_id, authority_id) VALUES (
  (SELECT id
   FROM pipouser
   WHERE email = 'email2@email.com'),
  (SELECT id
   FROM authority
   WHERE name = 'USER'));