UPDATE PIPOUSER
SET password = '$2a$10$x6UyaPBxjn64zet.pC1avOPUwXm4/h.gGyZi7TZx2GQJn/WDkcXee'
WHERE EMAIL = 'email@email.com';


UPDATE PIPOUSER
SET password = '$2a$10$GBqmsHR6SfY4bgj6fPASteHE3TSaHAMYM0QiC63kfoMKCv7QB/hg.'
WHERE EMAIL = 'email2@email.com';