package com.bitfingers;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BitFingers {

	public static void main(String[] args) {
		SpringApplication.run(BitFingers.class, args);
	}
}
