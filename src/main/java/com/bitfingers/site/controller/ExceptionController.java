package com.bitfingers.site.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ExceptionController {
    private static final String ERROR_PATH = "error";

    @ExceptionHandler(value = {ArithmeticException.class})
    public String handleArithmeticException() {
        //you can add may models here and show them on the
        //page
        return ERROR_PATH + "/dividebyone";
    }
}
