package com.bitfingers.site.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring4.SpringTemplateEngine;

@Controller
public class HomeController {

    private static int counter = 0;

    @Autowired
    SpringTemplateEngine springTemplateEngine;

    @GetMapping(value = {"/","/home"})
    public String home() {
        return "home";
    }


    public String sendMail() {
        if (counter < 2) {
            sendConfirmationEmail();
        }
        counter++;
        return "home";
    }


    public String evil() {
        double i = 1 / 0;
        return "home";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/logout")
    public String logout() {
        return "home";
    }

    @GetMapping("/hello")
    public String hello() {
        return "hello";
    }

    private void sendConfirmationEmail() {
        MimeMessagePreparator preparedMessage = mimeMessage -> {
            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
            message.setSubject("SampleDemoMessage");
            message.setFrom("bitfingers@gmail.com");
            message.setTo("leebangalee@gmail.com");
            Context ctx = new Context();
            ctx.setVariable("metName", "Sample message saying hello from me");
            ctx.setVariable("clazzName", "Hello from the developer");
            String emailText = springTemplateEngine.process("mail/samplemail", ctx);
            message.setText(emailText, true);
        };
    }

}

