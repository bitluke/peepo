@org.hibernate.annotations.GenericGenerator(
  name = "demo_id_generator",
  strategy = "enhanced-sequence",
  parameters = {
     @org.hibernate.annotations.Parameter(
        name = "sequence_name",
        value = "demo_sequence"
     ),
     @org.hibernate.annotations.Parameter(
        name = "initial_value",
        value = "1000"
     )
})
package com.bitfingers.site.base;
