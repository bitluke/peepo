package com.bitfingers.site.usermanagement.authority;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AuthorityService {

    @Autowired
    private AuthorityRepository authorityRepository;

    public Authority save(Authority authorityToSave) {
        return authorityRepository.save(authorityToSave);
    }

    public Authority update(Authority authorityToUpdate) {
        return authorityRepository.save(authorityToUpdate);
    }

    public List<Authority> findAll() {
        return authorityRepository.findAll();
    }

    public Page<Authority> findAll(Pageable pageable) {
        return authorityRepository.findAll(pageable);
    }

    public Authority findById(Long id) {
        return authorityRepository.findOne(id);
    }

    public Page<Authority> findAll(int page, int size) {
        return authorityRepository.findAll(new PageRequest(page, size));
    }

    public void delete(Authority authorityToUpdate) {
        authorityRepository.delete(authorityToUpdate);
    }

    public void delete(Long id) {
        authorityRepository.delete(id);
    }

    public List<Authority> findbyName(String name) {
        return authorityRepository.findByName(name);
    }
}
