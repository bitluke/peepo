package com.bitfingers.site.usermanagement.authority;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {

    public List<Authority> findByName(String name);
    public List<Authority> findByDescription(String description);

}
