package com.bitfingers.site.usermanagement.authority;

import com.bitfingers.site.base.BaseEntity;
import com.bitfingers.site.usermanagement.pipouser.PipoUser;
import org.hibernate.envers.Audited;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "AUTHORITY")
@Audited
public class Authority extends BaseEntity {
    @NotBlank
    private String name;
    @NotBlank
    private String description;

    @ManyToMany(mappedBy = "authorities")
    private List<PipoUser> users;

    public Authority() {
        this("","");
    }

    public Authority(String name, String description) {
        this.name =name;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<PipoUser> getUsers() {
        return users;
    }

    public void setUsers(List<PipoUser> users) {
        this.users = users;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Authority authority = (Authority) o;

        if (!name.equals(authority.name)) return false;
        return description.equals(authority.description);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + description.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Authority{");
        sb.append("name='").append(name).append('\'');
        sb.append(", description='").append(description).append('\'');
        sb.append(", users=").append(users);
        sb.append('}');
        return sb.toString();
    }
}
