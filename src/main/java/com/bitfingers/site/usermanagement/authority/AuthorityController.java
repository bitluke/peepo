package com.bitfingers.site.usermanagement.authority;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class AuthorityController {

    public static final String USER_MANAGEMENT_AUTHORITY_PATH = "usermanagement/authority";
    private AuthorityService authorityService;

    @Autowired
    public AuthorityController(AuthorityService authorityService) {
        this.authorityService = authorityService;
    }

    @ModelAttribute
    public Authority authority(){
        return new Authority();
    }

    @GetMapping("/authority")
    public String authorityForm(){
        return USER_MANAGEMENT_AUTHORITY_PATH + "/form";
    }

    @PostMapping("/authority")
    public String saveAuthority(@Valid Authority authority,
                       final BindingResult bindingResult,Model model){

        if (bindingResult.hasErrors()){
            return USER_MANAGEMENT_AUTHORITY_PATH + "/form";
        }

        model.addAttribute("id",authorityService.save(authority).getId());
        return "redirect:/authority/{id}";
    }

    @GetMapping("/authorities")
    public String listAuthorities(Model model, @PageableDefault(size = 1)Pageable pageable){
        Page<Authority> page = authorityService.findAll(pageable);
        model.addAttribute("page", page);
        return USER_MANAGEMENT_AUTHORITY_PATH + "/list";
    }


    @GetMapping("/authority/{id}")
    public String view(@PathVariable("id") long id, Model model){
        model.addAttribute(authorityService.findById(id));
        return USER_MANAGEMENT_AUTHORITY_PATH + "/view";
    }

    @GetMapping("/authority/{id}/delete")
    public String delete(@PathVariable("id") long id){
        authorityService.delete(id);
        return "redirect:/authorities";
    }

    @GetMapping("/authority/{id}/edit")
    public String editPage(@PathVariable("id") long id, Model model){
        model.addAttribute(authorityService.findById(id));
        return USER_MANAGEMENT_AUTHORITY_PATH + "/form";
    }

}
