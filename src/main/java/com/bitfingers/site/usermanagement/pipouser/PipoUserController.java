package com.bitfingers.site.usermanagement.pipouser;


import com.bitfingers.site.usermanagement.authority.Authority;
import com.bitfingers.site.usermanagement.authority.AuthorityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.IntStream;

@Controller
public class PipoUserController {

    public static final String USER_MANAGEMENT_PIPOUSER_PATH = "usermanagement/pipouser";

    private PipoUserService pipoUserService;

    @Autowired
    private AuthorityService authorityService;

    @Autowired
    public PipoUserController(PipoUserService pipoUserService) {
        this.pipoUserService = pipoUserService;
    }

    @ModelAttribute
    public PipoUser pipousers(){
        return new PipoUser();
    }

    @ModelAttribute
    public Sex[] sexes(){
        return Sex.values();
    }

    @ModelAttribute
    public List<Authority> authorities(){
        return authorityService.findAll();
    }

    @ModelAttribute
    public int[] ages(){
        return IntStream.range(0, 100).map(i -> i + 1).toArray();
    }

    @GetMapping("/user")
    public String pipoUserForm(){
        return USER_MANAGEMENT_PIPOUSER_PATH + "/form";
    }

    @PostMapping("/user")
    public String savePipoUser(@Valid PipoUser pipouser,
                                final BindingResult bindingResult, Model model){

        if (bindingResult.hasErrors()){
            return USER_MANAGEMENT_PIPOUSER_PATH + "/form";
        }

        model.addAttribute("id",pipoUserService.save(pipouser).getId());
        return "redirect:/user/{id}";
    }

    @GetMapping("/users")
    public String listUsers(Model model, @PageableDefault(size = 1)Pageable pageable){
        Page<PipoUser> page = pipoUserService.findAll(pageable);
        model.addAttribute("page", page);
        return USER_MANAGEMENT_PIPOUSER_PATH + "/list";
    }


    @GetMapping("/user/{id}")
    public String view(@PathVariable("id") long id, Model model){
        model.addAttribute(pipoUserService.findById(id));
        return USER_MANAGEMENT_PIPOUSER_PATH + "/view";
    }

    @GetMapping("/user/{id}/delete")
    public String delete(@PathVariable("id") long id){
        pipoUserService.delete(id);
        return "redirect:/users";
    }

    @GetMapping("/user/{id}/edit")
    public String editPage(@PathVariable("id") long id, Model model){
        model.addAttribute(pipoUserService.findById(id));
        return USER_MANAGEMENT_PIPOUSER_PATH + "/form";
    }

}
