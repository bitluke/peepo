package com.bitfingers.site.usermanagement.pipouser;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PipoUserRepository extends JpaRepository<PipoUser, Long> {

    List<PipoUser> findByLastname(String lastName);
}
