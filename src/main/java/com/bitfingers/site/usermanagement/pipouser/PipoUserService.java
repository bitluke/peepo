package com.bitfingers.site.usermanagement.pipouser;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PipoUserService {

    @Autowired
    private PipoUserRepository pipoUserRepository;

    public PipoUser save(PipoUser authorityToSave) {
        return pipoUserRepository.save(authorityToSave);
    }

    public PipoUser update(PipoUser authorityToUpdate) {
        return pipoUserRepository.save(authorityToUpdate);
    }

    public List<PipoUser> findAll() {
        return pipoUserRepository.findAll();
    }

    public Page<PipoUser> findAll(Pageable pageable) {
        return pipoUserRepository.findAll(pageable);
    }

    public PipoUser findById(Long id) {
        return pipoUserRepository.findOne(id);
    }

    public Page<PipoUser> findAll(int page, int size) {
        return pipoUserRepository.findAll(new PageRequest(page, size));
    }


    public void delete(Long id) {
        pipoUserRepository.delete(id);
    }

}
