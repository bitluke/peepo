package com.bitfingers.site.usermanagement.pipouser;

import com.bitfingers.site.base.BaseEntity;
import com.bitfingers.site.usermanagement.authority.Authority;
import org.hibernate.envers.Audited;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.List;

@Entity
@Audited
@Table(name = "PIPOUSER")
public class PipoUser extends BaseEntity {
    private String username;
    private String firstname;
    private String lastname;
    private String middlename;

    @Column(name = "PASSWORD")
    private String password;
    private String address;
    private String city;
    private Boolean active;
    @Pattern(regexp = ".+@.+\\.[a-z]+")
    @Size(min = 3, max = 255)
    @Basic(optional = false)
    @Column(name = "EMAIL", unique = true)
    private String email;
    private Long age;
    @Enumerated(EnumType.STRING)
    private Sex sex;

    @JoinTable(name = "PIPOUSER_X_AUTHORITY",
            joinColumns = {
                    @JoinColumn(name = "PIPOUSER_ID", referencedColumnName = "ID")},
            inverseJoinColumns = {
                    @JoinColumn(name = "AUTHORITY_ID", referencedColumnName = "ID")}
    )
    @ManyToMany
    protected List<Authority> authorities;

    public List<Authority> getAuthorities() {
        return authorities;
    }

    public void addAuthority(Authority authority) {
        if (authority == null) {
            authorities = new ArrayList<>();
        }
        authorities.add(authority);
    }

    public void setAuthorities(List<Authority> authorities) {
        this.authorities = authorities;
    }

    public PipoUser() {
        this("","","","");
    }


    public PipoUser(String username, String firstname, String lastname, String email) {
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Long getId() {
        return super.getId();
    }

    public void setId(Long id) {
        super.setId(id);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getAge() {
        return age;
    }

    public void setAge(Long age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PipoUser pipoUser = (PipoUser) o;

        if (!username.equals(pipoUser.username)) return false;
        if (!firstname.equals(pipoUser.firstname)) return false;
        if (!lastname.equals(pipoUser.lastname)) return false;
        if (middlename != null ? !middlename.equals(pipoUser.middlename) : pipoUser.middlename != null) return false;
        return email.equals(pipoUser.email);
    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + firstname.hashCode();
        result = 31 * result + lastname.hashCode();
        result = 31 * result + (middlename != null ? middlename.hashCode() : 0);
        result = 31 * result + email.hashCode();
        return result;
    }
}