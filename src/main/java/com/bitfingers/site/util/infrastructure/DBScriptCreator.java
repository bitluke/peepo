package com.bitfingers.site.util.infrastructure;

import java.io.BufferedOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class DBScriptCreator {

    public static void main(String[] args) {
        String defaultScriptPurpose = "no_purpose";
        DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
        String formattedDateTime = LocalDateTime.ofInstant(Instant.now(), ZoneId.systemDefault()).format(dateTimeFormat);
        String filename = "V" + formattedDateTime + "__" + defaultScriptPurpose + ".changetosql";
        try (FileOutputStream fileOutputStream = new FileOutputStream(filename)) {
            BufferedOutputStream bof = new BufferedOutputStream(fileOutputStream);
            bof.flush();
        } catch (FileNotFoundException fnfe) {
            System.err.println("File not found");
        } catch (IOException ioe) {
            System.err.println("Problem with IO");
        }
    }
}
