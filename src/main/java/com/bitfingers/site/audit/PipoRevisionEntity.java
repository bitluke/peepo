package com.bitfingers.site.audit;

import com.bitfingers.site.util.Constants;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.envers.ModifiedEntityNames;
import org.hibernate.envers.RevisionEntity;
import org.hibernate.envers.RevisionNumber;
import org.hibernate.envers.RevisionTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name="REVINFO")
@RevisionEntity(PipoRevisionListener.class)
public class PipoRevisionEntity implements Serializable{

    @Id
    @RevisionNumber
    @GeneratedValue(generator = Constants.ID_GENERATOR)
    @Column(name="REV")
    private Long rev;

    @RevisionTimestamp
    @Column(name="REVTSTMP")
    private Long revisonTimeStamp;

    @Column(name="USERNAME")
    private String username;

    public String getUsername() { return username; }
    public void setUsername( String username ) { this.username = username; }


    @Transient
    public Date getRevisionDate() {
        return new Date( revisonTimeStamp );
    }

    public Long getRev() {
        return rev;
    }

    public void setRev(Long rev) {
        this.rev = rev;
    }

    public Long getRevisonTimeStamp() {
        return revisonTimeStamp;
    }

    public void setRevisonTimeStamp(Long revisonTimeStamp) {
        this.revisonTimeStamp = revisonTimeStamp;
    }

    @ElementCollection(fetch = FetchType.EAGER)
    @JoinTable(name = "REVCHANGES", joinColumns = @JoinColumn(name = "REV"))
    @Column(name = "ENTITYNAME")
    @Fetch(FetchMode.JOIN)
    @ModifiedEntityNames
    private Set<String> modifiedEntityNames = new HashSet<String>();

    public Set<String> getModifiedEntityNames() {
        return modifiedEntityNames;
    }

    public void setModifiedEntityNames(Set<String> modifiedEntityNames) {
        this.modifiedEntityNames = modifiedEntityNames;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PipoRevisionEntity that = (PipoRevisionEntity) o;

        if (!rev.equals(that.rev)) return false;
        if (!revisonTimeStamp.equals(that.revisonTimeStamp)) return false;
        if (!username.equals(that.username)) return false;
        return modifiedEntityNames.equals(that.modifiedEntityNames);
    }

    @Override
    public int hashCode() {
        int result = rev.hashCode();
        result = 31 * result + revisonTimeStamp.hashCode();
        result = 31 * result + username.hashCode();
        result = 31 * result + modifiedEntityNames.hashCode();
        return result;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("PipoRevisionEntity{");
        sb.append("rev=").append(rev);
        sb.append(", revisonTimeStamp=").append(revisonTimeStamp);
        sb.append(", username='").append(username).append('\'');
        sb.append(", modifiedEntityNames=").append(modifiedEntityNames);
        sb.append('}');
        return sb.toString();
    }
}
