package com.bitfingers.site.audit;

import com.bitfingers.site.util.SecurityUtil;
import org.hibernate.envers.RevisionListener;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class PipoRevisionListener implements RevisionListener {


    @Override
    public void newRevision(Object revisionEntity) {
        PipoRevisionEntity pipoRevisionEntity = (PipoRevisionEntity) revisionEntity;
        pipoRevisionEntity.setUsername(SecurityUtil.getUsername());
        Date insertionDate = new Date();
        pipoRevisionEntity.setRevisonTimeStamp(insertionDate.toInstant().getEpochSecond());
    }
}
