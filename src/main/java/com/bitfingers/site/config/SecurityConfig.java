package com.bitfingers.site.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    public static final String DEF_USERS_BY_USERNAME_QUERY =
            "select username, password, active " +
                    "from PIPOUSER " +
                    "where email = ?";

    public static final String DEF_AUTHORITIES_BY_USERNAME_QUERY =
            "select pu.username, au.name " +
                    "from PIPOUSER pu " +
                    "join PIPOUSER_X_AUTHORITY puxa on puxa.pipouser_id = pu.id " +
                    "join AUTHORITY au on au.id = puxa.authority_id " +
                    "where pu.email = ?";
    public static final String REMEMBER_ME_KEY = "y17Qi6JIhQXzUsEtSz0g";

    private DataSource dataSource;

    @Autowired
    public SecurityConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin().loginPage("/login").and()
                .logout().and()
                .authorizeRequests()
                .antMatchers("/", "/home", "/hello", "/index",
                        "/static/fonts/**", "/static/js/**", "/static/img/**",
                        "/static/css/**").permitAll()
                .antMatchers("/greeting").authenticated()
                .anyRequest().permitAll().and()
                .rememberMe()
                .key(REMEMBER_ME_KEY)
                .tokenRepository(persistentTokenRepository())
                .tokenValiditySeconds(300);
    }

    private PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl jdbcTokenRepository = new JdbcTokenRepositoryImpl();
        jdbcTokenRepository.setDataSource(dataSource);
        return jdbcTokenRepository;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(dataSource)
                .usersByUsernameQuery(DEF_USERS_BY_USERNAME_QUERY)
                .authoritiesByUsernameQuery(DEF_AUTHORITIES_BY_USERNAME_QUERY)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

}