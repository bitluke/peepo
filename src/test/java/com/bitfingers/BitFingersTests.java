package com.bitfingers;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.MatcherAssert.assertThat;

@RunWith(SpringRunner.class)
public class BitFingersTests {

	@Test
	public void contextLoads() {
		assertThat(1  * 1 , CoreMatchers.is(1));
	}

	@Test
	public void onePlusOneIsTwo() {
		assertThat(1 +1 , CoreMatchers.is(2));
	}

}
